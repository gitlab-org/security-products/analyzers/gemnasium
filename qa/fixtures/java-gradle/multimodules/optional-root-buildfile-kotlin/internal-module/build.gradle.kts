plugins {
    java
}

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("junit:junit:4.13")
    implementation("io.netty:netty:3.9.1.Final")
    implementation("org.apache.maven:maven-artifact:3.3.9")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.9.2")
}
