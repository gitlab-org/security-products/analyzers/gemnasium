#!/bin/sh

# checkout branch that will be updated in the image's clone
git -C /gemnasium-db checkout $ORIGINAL_REMOTE_STABLE_BRANCH

# create local remote
git clone --branch $ORIGINAL_REMOTE_STABLE_BRANCH https://gitlab.com/gitlab-org/security-products/gemnasium-db $GEMNASIUM_DB_REMOTE_URL

# ensure local remote has all the needed original branches for this test
git -C $GEMNASIUM_DB_REMOTE_URL branch --track $ORIGINAL_REMOTE_OLDER_STABLE_BRANCH "origin/$ORIGINAL_REMOTE_OLDER_STABLE_BRANCH"

mkdir -p $GEMNASIUM_DB_REMOTE_URL/go/github.com/sirupsen/logrus
cat > $GEMNASIUM_DB_REMOTE_URL/go/github.com/sirupsen/logrus/fake-vuln.yml << END_YAML
identifier: "fake-vuln"
package_slug: "github.com/sirupsen/logrus"
title: "fake vuln to demonstrate scan time advisory db updates"
description: "fake vuln to demonstrate scan time advisory db updates"
date: "2021-01-29"
pubdate: "2021-01-29"
affected_range: "=v1.4.2"
fixed_versions:
- "1.4.2"
affected_versions: "All versions before fake one"
not_impacted: "All versions starting after fake one"
solution: "Upgrade to version after fake one."
urls:
- "https://nvd.nist.gov/vuln/detail/fake-vuln"
cvss_v2: "AV:N/AC:M/Au:N/C:P/I:P/A:N"
cvss_v3: "CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N"
uuid: "f8d39948-1678-453e-87f5-854c53bc2d5b"
END_YAML
git -C $GEMNASIUM_DB_REMOTE_URL add go/github.com/sirupsen/logrus
git -C $GEMNASIUM_DB_REMOTE_URL commit -m "Add fake vuln"

# create new tag and branch
git -C $GEMNASIUM_DB_REMOTE_URL tag $NEW_TAG_NAME
git -C $GEMNASIUM_DB_REMOTE_URL checkout -b $NEW_BRANCH_NAME
