{
  "version": "15.0.4",
  "vulnerabilities": [
    {
      "id": "55779663889d647072d98d702e5064dd383749533322fc9d2407a69c4d8e3b1f",
      "name": "OS Command Injection",
      "description": "Attackers could trick execa into executing arbitrary binaries. This behaviour is caused by the setting `preferLocal=true` which makes execa search for locally installed binaries and executes them.",
      "severity": "Critical",
      "solution": "Upgrade to version 2.0.0 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "execa"
          },
          "version": "0.7.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-05cfa2e8-2d0c-42c1-8894-638e2f12ff3d",
          "value": "05cfa2e8-2d0c-42c1-8894-638e2f12ff3d",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/execa/GMS-2020-2.yml"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "Unknown",
          "vector": "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
        },
        {
          "vendor": "Unknown",
          "vector": "AV:N/AC:L/Au:N/C:C/I:C/A:C"
        }
      ],
      "links": [
        {
          "url": "https://github.com/sindresorhus/execa/releases/tag/v2.0.0"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "execa:0.7.0"
        }
      }
    },
    {
      "id": "5cfd6a880b2d83cc2e94e21a6ed16a07121cda1ee79ee3ce1d22abb91727e57b",
      "name": "Improper Input Validation",
      "description": "lodash is vulnerable to Prototype Pollution. The function `defaultsDeep` could be tricked into adding or modifying properties of `Object.prototype` using a constructor payload.",
      "severity": "Critical",
      "solution": "Upgrade to version 4.17.12 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "lodash"
          },
          "version": "4.17.10"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-4774cd67-936f-419e-8533-ae5cfe7db9f9",
          "value": "4774cd67-936f-419e-8533-ae5cfe7db9f9",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/lodash/CVE-2019-10744.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-10744",
          "value": "CVE-2019-10744",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-10744"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:L/Au:N/C:P/I:P/A:P"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-10744"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "lodash:4.17.10"
        }
      }
    },
    {
      "id": "a4d3a8a585679704f759c18554ac7b04659daba9c343f8b4dc1e8368ef0abc84",
      "name": "Uncontrolled Resource Consumption",
      "description": "A prototype pollution vulnerability was found in lodash where the functions `merge`, `mergeWith`, and `defaultsDeep` can be tricked into adding or modifying properties of `Object.prototype`.",
      "severity": "Critical",
      "solution": "Upgrade to version 4.17.11 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "lodash"
          },
          "version": "4.17.10"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-d8822263-8a6f-43ea-bb6b-7a2a0cabdf5c",
          "value": "d8822263-8a6f-43ea-bb6b-7a2a0cabdf5c",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/lodash/CVE-2018-16487.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2018-16487",
          "value": "CVE-2018-16487",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-16487"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:L/Au:N/C:P/I:P/A:P"
        }
      ],
      "links": [
        {
          "url": "https://hackerone.com/reports/380873"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2018-16487"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "lodash:4.17.10"
        }
      }
    },
    {
      "id": "6bc40fb3eeff74b1edb639c82303a205e2bf1fdc87c188b245969e7471375fd3",
      "name": "Argument Injection or Modification",
      "description": "mixin-deep is vulnerable to Prototype Pollution. The function mixin-deep could be tricked into adding or modifying properties of `Object.prototype` using a constructor payload.",
      "severity": "Critical",
      "solution": "Upgrade to versions 1.3.2, 2.0.1 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "mixin-deep"
          },
          "version": "1.3.1"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-04af00f2-3b09-4656-9f4b-bcb2f4ef3db1",
          "value": "04af00f2-3b09-4656-9f4b-bcb2f4ef3db1",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/mixin-deep/CVE-2019-10746.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-10746",
          "value": "CVE-2019-10746",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-10746"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:L/Au:N/C:P/I:P/A:P"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-10746"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "mixin-deep:1.3.1"
        }
      }
    },
    {
      "id": "127a8054ab9c8e686fdd50f6c67471d9f7d8c3653f0e25d158455c3cca75f13f",
      "name": "Uncontrolled Resource Consumption",
      "description": "set-value is vulnerable to Prototype Pollution. The function `mixin-deep` could be tricked into adding or modifying properties of `Object.prototype` using any of the constructor, prototype and `_proto_` payloads.",
      "severity": "Critical",
      "solution": "Upgrade to versions 2.0.1, 3.0.1 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "set-value"
          },
          "version": "0.4.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-6f541fe7-9711-457a-8003-a52d8650b66f",
          "value": "6f541fe7-9711-457a-8003-a52d8650b66f",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/set-value/CVE-2019-10747.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-10747",
          "value": "CVE-2019-10747",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-10747"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:L/Au:N/C:P/I:P/A:P"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-10747"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "set-value:0.4.3"
        }
      }
    },
    {
      "id": "78e47332de892b3c38bb5d969a718e95b8ebf87deb80d70e96d1c5a636be4fb9",
      "name": "Uncontrolled Resource Consumption",
      "description": "set-value is vulnerable to Prototype Pollution. The function `mixin-deep` could be tricked into adding or modifying properties of `Object.prototype` using any of the constructor, prototype and `_proto_` payloads.",
      "severity": "Critical",
      "solution": "Upgrade to versions 2.0.1, 3.0.1 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "set-value"
          },
          "version": "2.0.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-6f541fe7-9711-457a-8003-a52d8650b66f",
          "value": "6f541fe7-9711-457a-8003-a52d8650b66f",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/set-value/CVE-2019-10747.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-10747",
          "value": "CVE-2019-10747",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-10747"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:L/Au:N/C:P/I:P/A:P"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-10747"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "set-value:2.0.0"
        }
      }
    },
    {
      "id": "8771ebd56ce731156ded99a4e9398a8fbd6ff6f8a1871aa203ace803146df3d8",
      "name": "Regular Expression Denial of Service",
      "description": "A regex in the form of `/[x-\\ud800]/u` causes the parser to enter an infinite loop. The string is not valid UTF16 which usually results in it being sanitized before reaching the parser.",
      "severity": "High",
      "solution": "Upgrade to versions 5.7.4, 6.4.1, 7.1.1 or later.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "acorn"
          },
          "version": "5.7.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-cd6cfe67-4650-49c6-a10a-ee4195a573fa",
          "value": "cd6cfe67-4650-49c6-a10a-ee4195a573fa",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/acorn/GMS-2020-1.yml"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "Unknown",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H"
        }
      ],
      "links": [
        {
          "url": "https://github.com/acornjs/acorn/commit/793c0e569ed1158672e3a40aeed1d8518832b802"
        },
        {
          "url": "https://snyk.io/vuln/SNYK-JS-ACORN-559469"
        },
        {
          "url": "https://www.npmjs.com/advisories/1488"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "acorn:5.7.3"
        }
      }
    },
    {
      "id": "1d8799d916ec2dbfa9ef8ed5adf9e0780f75f6304a297b56fc66b69a780509e7",
      "name": "Regular Expression Denial of Service",
      "description": "A regex in the form of `/[x-\\ud800]/u` causes the parser to enter an infinite loop. The string is not valid UTF16 which usually results in it being sanitized before reaching the parser.",
      "severity": "High",
      "solution": "Upgrade to versions 5.7.4, 6.4.1, 7.1.1 or later.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "acorn"
          },
          "version": "6.4.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-cd6cfe67-4650-49c6-a10a-ee4195a573fa",
          "value": "cd6cfe67-4650-49c6-a10a-ee4195a573fa",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/acorn/GMS-2020-1.yml"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "Unknown",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H"
        }
      ],
      "links": [
        {
          "url": "https://github.com/acornjs/acorn/commit/793c0e569ed1158672e3a40aeed1d8518832b802"
        },
        {
          "url": "https://snyk.io/vuln/SNYK-JS-ACORN-559469"
        },
        {
          "url": "https://www.npmjs.com/advisories/1488"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "acorn:6.4.0"
        }
      }
    },
    {
      "id": "86f5756c7d17ce2112126c12463230684a2dee53b8b4c991b5bad567c9a6a90b",
      "name": "Improper Input Validation",
      "description": "Decamelize uses regular expressions to evaluate a string and takes unescaped separator values, which can be used to create a denial of service attack.",
      "severity": "High",
      "solution": "Upgrade to version 1.1.2 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "decamelize"
          },
          "version": "1.1.1"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-cfa05151-753f-4373-833c-a27e772c87a2",
          "value": "cfa05151-753f-4373-833c-a27e772c87a2",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/decamelize/CVE-2017-16023.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2017-16023",
          "value": "CVE-2017-16023",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-16023"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:L/Au:N/C:N/I:N/A:P"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2017-16023"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "decamelize:1.1.1"
        }
      }
    },
    {
      "id": "cf75f91bb8fea95dff1c22cfdac836e9cb6d4dd6146c5388048bc1584e4bc947",
      "name": "Type checking vulnerability",
      "description": "`ctorName` allows external user input to overwrite certain internal attributes via a conflicting name.",
      "severity": "High",
      "solution": "Upgrade to version 6.0.3 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "kind-of"
          },
          "version": "3.2.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-c69c3883-e67c-4be5-a4dd-ce1d82173049",
          "value": "c69c3883-e67c-4be5-a4dd-ce1d82173049",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/kind-of/CVE-2019-20149.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-20149",
          "value": "CVE-2019-20149",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-20149"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:H/A:N"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:L/Au:N/C:N/I:P/A:N"
        }
      ],
      "links": [
        {
          "url": "https://github.com/jonschlinkert/kind-of/issues/30"
        },
        {
          "url": "https://github.com/jonschlinkert/kind-of/pull/31"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-20149"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "kind-of:3.2.2"
        }
      }
    },
    {
      "id": "bd74527c605479c0a91ba872bb1c93828c637427ac3a53f15f6e51d66abca2a7",
      "name": "Type checking vulnerability",
      "description": "`ctorName` allows external user input to overwrite certain internal attributes via a conflicting name.",
      "severity": "High",
      "solution": "Upgrade to version 6.0.3 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "kind-of"
          },
          "version": "4.0.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-c69c3883-e67c-4be5-a4dd-ce1d82173049",
          "value": "c69c3883-e67c-4be5-a4dd-ce1d82173049",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/kind-of/CVE-2019-20149.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-20149",
          "value": "CVE-2019-20149",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-20149"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:H/A:N"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:L/Au:N/C:N/I:P/A:N"
        }
      ],
      "links": [
        {
          "url": "https://github.com/jonschlinkert/kind-of/issues/30"
        },
        {
          "url": "https://github.com/jonschlinkert/kind-of/pull/31"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-20149"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "kind-of:4.0.0"
        }
      }
    },
    {
      "id": "0430bfdccc610c8fb380bd5f10bdbf8d73b6124ea69b97ad6cfcb379f4ae840c",
      "name": "Type checking vulnerability",
      "description": "`ctorName` allows external user input to overwrite certain internal attributes via a conflicting name.",
      "severity": "High",
      "solution": "Upgrade to version 6.0.3 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "kind-of"
          },
          "version": "5.1.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-c69c3883-e67c-4be5-a4dd-ce1d82173049",
          "value": "c69c3883-e67c-4be5-a4dd-ce1d82173049",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/kind-of/CVE-2019-20149.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-20149",
          "value": "CVE-2019-20149",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-20149"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:H/A:N"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:L/Au:N/C:N/I:P/A:N"
        }
      ],
      "links": [
        {
          "url": "https://github.com/jonschlinkert/kind-of/issues/30"
        },
        {
          "url": "https://github.com/jonschlinkert/kind-of/pull/31"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-20149"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "kind-of:5.1.0"
        }
      }
    },
    {
      "id": "5a9cc0ffe63585f58560e0a2b43118bc50f676884759af6e212399d9d8c4c2bd",
      "name": "Type checking vulnerability",
      "description": "`ctorName` allows external user input to overwrite certain internal attributes via a conflicting name.",
      "severity": "High",
      "solution": "Upgrade to version 6.0.3 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "kind-of"
          },
          "version": "6.0.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-c69c3883-e67c-4be5-a4dd-ce1d82173049",
          "value": "c69c3883-e67c-4be5-a4dd-ce1d82173049",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/kind-of/CVE-2019-20149.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-20149",
          "value": "CVE-2019-20149",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-20149"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:H/A:N"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:L/Au:N/C:N/I:P/A:N"
        }
      ],
      "links": [
        {
          "url": "https://github.com/jonschlinkert/kind-of/issues/30"
        },
        {
          "url": "https://github.com/jonschlinkert/kind-of/pull/31"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-20149"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "kind-of:6.0.2"
        }
      }
    },
    {
      "id": "b9c6396aeae7dbe478dc5058e12e9500ad23c9162025526328ff52db6a7b2fbc",
      "name": "Object Prototype Pollution",
      "description": "Prototype pollution attack when using `_.zipObjectDeep` in lodash.",
      "severity": "High",
      "solution": "Upgrade to version 4.17.20 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "lodash"
          },
          "version": "4.17.10"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-1f7fa42b-6b17-46b7-88a5-8995b43d298f",
          "value": "1f7fa42b-6b17-46b7-88a5-8995b43d298f",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/lodash/CVE-2020-8203.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-8203",
          "value": "CVE-2020-8203",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-8203"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:H"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:M/Au:N/C:N/I:P/A:P"
        }
      ],
      "links": [
        {
          "url": "https://github.com/lodash/lodash/issues/4874"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-8203"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "lodash:4.17.10"
        }
      }
    },
    {
      "id": "e02579e06e4cd6f8e07c70a3d7b1a3b14d96e02a01e0fe012b7a633922c7abbd",
      "name": "Object Prototype Pollution",
      "description": "Prototype pollution attack when using `_.zipObjectDeep` in lodash.",
      "severity": "High",
      "solution": "Upgrade to version 4.17.20 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "lodash"
          },
          "version": "4.17.15"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-1f7fa42b-6b17-46b7-88a5-8995b43d298f",
          "value": "1f7fa42b-6b17-46b7-88a5-8995b43d298f",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/lodash/CVE-2020-8203.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-8203",
          "value": "CVE-2020-8203",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-8203"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:H/A:H"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:M/Au:N/C:N/I:P/A:P"
        }
      ],
      "links": [
        {
          "url": "https://github.com/lodash/lodash/issues/4874"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-8203"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "lodash:4.17.15"
        }
      }
    },
    {
      "id": "eb51d8fbf5c21a5559c16f26b0f024339e37b31d5fe19807e1d08cf9cb222ceb",
      "name": "Uncontrolled Resource Consumption",
      "description": "lodash is affected by Uncontrolled Resource Consumption which can lead to a denial of service.",
      "severity": "Medium",
      "solution": "Upgrade to version 4.17.11 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "lodash"
          },
          "version": "4.17.10"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-109f3b4c-bdb3-48be-b2f9-e0348fba64bd",
          "value": "109f3b4c-bdb3-48be-b2f9-e0348fba64bd",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/lodash/CVE-2019-1010266.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-1010266",
          "value": "CVE-2019-1010266",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-1010266"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.0/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:L/Au:S/C:N/I:N/A:P"
        }
      ],
      "links": [
        {
          "url": "https://github.com/lodash/lodash/issues/3359"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-1010266"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "lodash:4.17.10"
        }
      }
    },
    {
      "id": "9e8f9bd9b19899636bfa97f4ea9f29cc204478de71f4bba03f192aa743b83c69",
      "name": "Improper Input Validation",
      "description": "minimist could be tricked into adding or modifying properties of `Object.prototype` using a `constructor` or `__proto__` payload.",
      "severity": "Medium",
      "solution": "Upgrade to version 1.2.2 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "minimist"
          },
          "version": "0.0.8"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-53e8766c-27eb-4278-8c4f-3dcef53a68bf",
          "value": "53e8766c-27eb-4278-8c4f-3dcef53a68bf",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/minimist/CVE-2020-7598.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-7598",
          "value": "CVE-2020-7598",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-7598"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:L/I:L/A:L"
        },
        {
          "vendor": "NVD",
          "vector": "AV:N/AC:M/Au:N/C:P/I:P/A:P"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-7598"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "minimist:0.0.8"
        }
      }
    },
    {
      "id": "caaadf3453b3d1101bd4eed3da2e0611406422c470958fb1d9006bdfa33a14bb",
      "name": "Improper Input Validation",
      "description": "yargs-parser could be tricked into adding or modifying properties of `Object.prototype` using a `__proto__` payload.",
      "severity": "Medium",
      "solution": "Upgrade to versions 13.1.2, 15.0.1, 18.1.1 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "yargs-parser"
          },
          "version": "8.1.0"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-a9e73366-2694-40b0-bcc1-795368307084",
          "value": "a9e73366-2694-40b0-bcc1-795368307084",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/yargs-parser/CVE-2020-7608.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-7608",
          "value": "CVE-2020-7608",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-7608"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:L"
        },
        {
          "vendor": "NVD",
          "vector": "AV:L/AC:L/Au:N/C:P/I:P/A:P"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-7608"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "yargs-parser:8.1.0"
        }
      }
    },
    {
      "id": "104b8bc8ea164f77972af8a42d627ce4b5ad40e33430084b8b3cf8fd558a32ab",
      "name": "Improper Input Validation",
      "description": "yargs-parser could be tricked into adding or modifying properties of `Object.prototype` using a `__proto__` payload.",
      "severity": "Medium",
      "solution": "Upgrade to versions 13.1.2, 15.0.1, 18.1.1 or above.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "yargs-parser"
          },
          "version": "9.0.2"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-a9e73366-2694-40b0-bcc1-795368307084",
          "value": "a9e73366-2694-40b0-bcc1-795368307084",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/yargs-parser/CVE-2020-7608.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-7608",
          "value": "CVE-2020-7608",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-7608"
        }
      ],
      "cvss_vectors": [
        {
          "vendor": "NVD",
          "vector": "CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:L"
        },
        {
          "vendor": "NVD",
          "vector": "AV:L/AC:L/Au:N/C:P/I:P/A:P"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-7608"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "yargs-parser:9.0.2"
        }
      }
    },
    {
      "id": "084449a2d123457ebe9c8336ee951664e2605ee4d30e410f40eb14804529da0a",
      "name": "Regular Expression Denial of Service",
      "description": "Decamelize uses regular expressions to evaluate a string and takes unescaped separator values, which can be used to create a denial of service attack.",
      "severity": "Unknown",
      "solution": "Upgrade to version 1.1.2 or later.",
      "location": {
        "file": "package-lock.json",
        "dependency": {
          "package": {
            "name": "decamelize"
          },
          "version": "1.1.1"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-980f208d-a755-46f3-8bac-4decf4c48f56",
          "value": "980f208d-a755-46f3-8bac-4decf4c48f56",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/npm/decamelize/GMS-2015-53.yml"
        }
      ],
      "links": [
        {
          "url": "https://github.com/sindresorhus/decamelize/issues/5"
        },
        {
          "url": "https://nodesecurity.io/advisories/308"
        }
      ],
      "details": {
        "vulnerable_package": {
          "type": "text",
          "name": "Vulnerable Package",
          "value": "decamelize:1.1.1"
        }
      }
    }
  ],
  "scan": {
    "analyzer": {
      "id": "gemnasium",
      "name": "Gemnasium",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
      "vendor": {
        "name": "GitLab"
      },
      "version": "4.7.0"
    },
    "scanner": {
      "id": "gemnasium",
      "name": "Gemnasium",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
      "vendor": {
        "name": "GitLab"
      },
      "version": "4.7.0"
    },
    "type": "dependency_scanning",
    "start_time": "2023-10-12T23:34:14",
    "end_time": "2023-10-12T23:34:22",
    "status": "success"
  }
}
