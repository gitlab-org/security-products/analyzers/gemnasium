package semver

import (
	"runtime"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange/cli"
)

func init() {
	cli.Register("maven", "semver/vrange-"+runtime.GOOS, "maven")
}
