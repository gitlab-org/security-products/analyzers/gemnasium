package swift

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange/gem"

// using the gem resolver because the versioning is the same for cocoapods
func init() {
	gem.Register("cocoapods")
}
