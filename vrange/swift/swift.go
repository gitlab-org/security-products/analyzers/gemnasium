package swift

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange/npm"

// using the npm resolver because the versioning is the same for swift
func init() {
	npm.Register("swift")
}
