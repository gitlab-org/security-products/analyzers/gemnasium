# A transient job failure during a release can delay image publication until the
# next successful run. Marking jobs prone to failure retryable aims to make
# our release process more resilient. By default only jobs on the default branch
# are retried.
#
# See: https://gitlab.com/gitlab-org/gitlab/-/issues/428139
#
# Example usage:
#
# my-job:
#   extends:
#     - .retryable
#   script:
#     - echo "hello, world!"
#     - exit 1

# We redefine .retryable under different circumstances in order to control its
# behaviour. This approach works around the limitation of the retry keyword,
# which does not support conditional logic.
include:
  - local: "/.gitlab/ci/retryable.retries.yml"
    rules:
      - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH =~ /^v\d$/'
        when: always
  # In merge requests, we anticipate that true failures may be common and using
  # a catch-all retry would spend CI minutes unnecessarily, so it is a noop. If
  # we don't include the noop local then .retryable will not be defined and the CI
  # config on non-default branch pipelines would be invalid.
  - local: "/.gitlab/ci/retryable.noop.yml"
    rules:
      - if: '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $CI_COMMIT_BRANCH !~ /^v\d$/'
        when: always
