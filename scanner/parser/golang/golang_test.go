package golang

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/testutil"
)

func TestParse_GoSum(t *testing.T) {
	types := []string{"small", "big", "duplicates", "malformed", "incompatible"}
	for _, tc := range types {
		t.Run(tc, func(t *testing.T) {
			dir := filepath.Join("gosum", tc)
			path := filepath.Join("fixtures", dir, "go.sum")
			fixture, err := os.Open(path)
			require.NoErrorf(t, err, "fixture not found %s", path)
			defer fixture.Close()

			got, _, err := Parse(fixture, parser.Options{})
			require.NoError(t, err)
			testutil.RequireExpectedPackages(t, dir, got)
		})
	}
}

func TestParse_GoProjectModulesJSON(t *testing.T) {
	types := []string{"default"}
	for _, tc := range types {
		t.Run(tc, func(t *testing.T) {
			dir := filepath.Join("gomod", tc)
			path := filepath.Join("fixtures", dir, "go-project-modules.json")
			fixture, err := os.Open(path)
			require.NoErrorf(t, err, "fixture not found %s", path)
			defer fixture.Close()

			got, _, err := Parse(fixture, parser.Options{})
			require.NoError(t, err)
			testutil.RequireExpectedPackages(t, dir, got)
		})
	}
}
