package uv

import (
	"io"

	dep_parser "gitlab.com/gitlab-org/security-products/analyzers/dependency-scanning/scanner/parser"
	uv_scanner "gitlab.com/gitlab-org/security-products/analyzers/dependency-scanning/scanner/parser/uv"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
)

// Parse scans a uv lock file and returns a list of packages
func Parse(r io.Reader, opts parser.Options) ([]parser.Package, []parser.Dependency, error) {
	packages, _, err := uv_scanner.Parse(r, r, dep_parser.Options{
		IncludeDev: opts.IncludeDev,
	})
	if err != nil {
		return nil, nil, err
	}
	packs := make([]parser.Package, 0, len(packages))
	for _, p := range packages {
		packs = append(packs, parser.Package{
			Name:    p.Name,
			Version: p.Version,
		})
	}
	return packs, nil, nil
}

func init() {
	parser.Register("uv", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypePypi,
		Filenames:   []string{"uv.lock"},
	})
}
