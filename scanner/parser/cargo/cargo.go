package cargo

import (
	"fmt"
	"io"
	"strings"

	"github.com/BurntSushi/toml"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
)

type lockfile struct {
	Package []struct {
		Name         string   `toml:"name"`
		Version      string   `toml:"version"`
		Dependencies []string `toml:"dependencies"`
	} `toml:"package"`
}

type packageNode struct {
	pkg          parser.Package
	hasDependent bool
}

type dependencyNode struct {
	from *packageNode
	to   *packageNode
}

// Parse scans a Cargo lock file and returns a list of packages and dependencies.
//
//	Aside from unmarshaling toml, 2 extra steps need to be done.
//	1. A package's dependencies list can refer to package by name only or by name and version.
//		The latter happens when different versions of the same package are needed. Name-only
//		dependencies are denoted by a single word while name and version are 2 words delimited by a space.
//	2. The application defined in the repository's Cargo.toml is listed as a regular [package] node interface
//		the lockfile. The parser finds and removes the application node by finding any nodes that do not have
//		incident edges.
func Parse(cargoLock io.Reader, _ parser.Options) ([]parser.Package, []parser.Dependency, error) {
	var lf lockfile

	if _, err := toml.NewDecoder(cargoLock).Decode(&lf); err != nil {
		return nil, nil, fmt.Errorf("unmarshaling Cargo.lock: %w", err)
	}

	// map lockfile entries by name, then version
	lfmap := make(map[string]map[string]*packageNode)

	pkgs := make([]*packageNode, 0, len(lf.Package))
	for _, p := range lf.Package {
		pkg := parser.Package{Name: p.Name, Version: p.Version}
		if _, ok := lfmap[p.Name]; !ok {
			lfmap[p.Name] = make(map[string]*packageNode)
		}
		node := &packageNode{pkg, false}
		lfmap[p.Name][p.Version] = node
		pkgs = append(pkgs, node)
	}

	deps := make([]dependencyNode, 0)
	for _, p := range lf.Package {
		from := lfmap[p.Name][p.Version]
		for _, depstr := range p.Dependencies {
			var to *packageNode
			// packages can be referred to by name or by name and version
			// figure out which one depstr is and parse accordingly
			depkeys := strings.Split(depstr, " ")
			if len(depkeys) == 1 {
				// find dependency referenced by name only
				pkgs, ok := lfmap[depkeys[0]]
				if !ok {
					return nil, nil, fmt.Errorf("dependencies list refers to an unknown package name %s", depkeys[0])
				}
				if len(pkgs) != 1 {
					return nil, nil, fmt.Errorf("lockfile should only have one instance of package %s", depkeys[0])
				}
				for _, pkg := range pkgs {
					to = pkg
				}
			} else {
				// find dependency referenced by name and version
				if _, ok := lfmap[depkeys[0]]; !ok {
					return nil, nil, fmt.Errorf("dependencies list refers to an unknown package name %s", depkeys[0])
				}
				pkg, ok := lfmap[depkeys[0]][depkeys[1]]
				if !ok {
					return nil, nil, fmt.Errorf("dependencies list refers to an unknown package version %s@%s", depkeys[0], depkeys[1])
				}
				to = pkg
			}
			to.hasDependent = true
			dep := dependencyNode{
				from: from,
				to:   to,
			}
			deps = append(deps, dep)
		}
	}

	// filter list of packages and remove nodes without dependents
	ppkgs := make([]parser.Package, 0)
	for _, pkg := range pkgs {
		if pkg.hasDependent {
			ppkgs = append(ppkgs, pkg.pkg)
		}
	}

	// filter list of dependencies and remove any edges from nodes without dependents
	ddeps := make([]parser.Dependency, 0)
	for _, dep := range deps {
		if dep.from.hasDependent {
			ddeps = append(ddeps, parser.Dependency{From: &dep.from.pkg, To: &dep.to.pkg})
		}
	}

	return ppkgs, ddeps, nil
}

func init() {
	parser.Register("cargo", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeCargo,
		Filenames:   []string{"Cargo.lock"},
	})
}
