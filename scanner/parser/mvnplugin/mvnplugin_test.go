package mvnplugin

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/testutil"
)

func TestMvnplugin(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		tcs := []struct {
			name           string // test case name
			fixtureDir     string // dir where fixtures are located
			fixture        string // fixture file name
			expectationDir string // dir where expectations are located
			opts           parser.Options
		}{
			{
				name:           "maven simple",
				fixtureDir:     "simple",
				fixture:        "maven-packages.json",
				expectationDir: "simple",
				opts:           parser.Options{IncludeDev: true},
			},
			{
				name:           "maven simple nodev",
				fixtureDir:     "simple",
				fixture:        "maven-packages.json",
				expectationDir: "simple-nodev",
				opts:           parser.Options{IncludeDev: false},
			},
			{
				name:           "maven big",
				fixtureDir:     "big",
				fixture:        "maven-packages.json",
				expectationDir: "big",
				opts:           parser.Options{IncludeDev: true},
			},
			{
				name:           "maven big nodev",
				fixtureDir:     "big",
				fixture:        "maven-packages.json",
				expectationDir: "big-nodev",
				opts:           parser.Options{IncludeDev: false},
			},
			{
				name:           "gradle",
				fixtureDir:     "gradle",
				fixture:        "gradle-dependencies.json",
				expectationDir: "gradle",
				opts:           parser.Options{IncludeDev: true},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc.fixtureDir, tc.fixture)
				got, _, err := Parse(fixture, tc.opts)
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tc.expectationDir, got)
			})
		}
	})
}
