package gradle

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestResolutionPolicy(t *testing.T) {
	t.Run("IsUnresolvable", func(t *testing.T) {
		tcs := []struct {
			resolutionPolicy string
			resolutionState  string
			want             bool
			err              error
		}{
			{
				resolutionPolicy: "",
				resolutionState:  "FAILED",
				want:             true,
			},
			{
				resolutionPolicy: "failed",
				resolutionState:  "FAILED",
				want:             true,
			},
			{
				resolutionPolicy: "failed",
				resolutionState:  "UNRESOLVED",
				want:             false,
			},
			{
				resolutionPolicy: "failed",
				resolutionState:  "RESOLVED",
				want:             false,
			},
			{
				resolutionPolicy: "none",
				resolutionState:  "FAILED",
				want:             false,
			},
			{
				resolutionPolicy: "none",
				resolutionState:  "UNRESOLVED",
				want:             false,
			},
			{
				resolutionPolicy: "none",
				resolutionState:  "RESOLVED",
				want:             false,
			},
			{
				resolutionPolicy: "unknown-policy",
				resolutionState:  "FAILED",
				err:              fmt.Errorf("unknown resolution policy: unknown-policy"),
			},
		}

		for _, tc := range tcs {
			t.Run(fmt.Sprintf("env: %s, state: %s", tc.resolutionPolicy, tc.resolutionState), func(t *testing.T) {
				policy, err := newResolutionPolicy(tc.resolutionPolicy)

				if tc.err != nil {
					require.Error(t, err)
					require.EqualError(t, err, tc.err.Error())
				} else {
					require.NoError(t, err)
					result := policy.violation(tc.resolutionState)
					require.Equal(t, tc.want, result)
				}
			})
		}
	})
}
