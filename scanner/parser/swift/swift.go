package swift

import (
	"encoding/json"
	"fmt"
	"io"
	"regexp"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
)

var repositoryURLRegex = regexp.MustCompile(`(?:https?://|git@)(.+?)(?:\.git)?$`)

// Root represents the top-level structure that can handle both versions
type lockfile struct {
	Packages []parser.Package
}

// findValue searches for a key in a nested map and returns the corresponding value.
func findValue(inputMap map[string]any, key string) (value any, found bool) {
	for k, v := range inputMap {
		if k == key {
			return v, true
		}
	}

	for _, v := range inputMap {
		if nestedMap, ok := v.(map[string]any); ok {
			if nestedValue, nestedFound := findValue(nestedMap, key); nestedFound {
				return nestedValue, nestedFound
			}
		}
	}

	return nil, false
}

// UnmarshalJSON custom unmarshal method to handle both versions of Package.resolved
func (l *lockfile) UnmarshalJSON(data []byte) error {
	if string(data) == "null" {
		return nil
	}

	m := make(map[string]any)
	if err := json.Unmarshal(data, &m); err != nil {
		return err
	}
	if pins, ok := findValue(m, "pins"); ok {

		for _, pin := range pins.([]any) {
			if pinMap, ok := pin.(map[string]any); ok {
				var p parser.Package
				name, err := packageName(pinMap)
				if err != nil {
					return err
				}
				p.Name = name
				p.Version = packageVersion(pinMap)
				l.Packages = append(l.Packages, p)

			}
		}
	}

	return nil
}

// packageVersion returns the version of the package
func packageVersion(m map[string]any) string {
	var version string
	if v, ok := findValue(m, "version"); ok {
		// Version can be a string or an nil
		if ver, ok := v.(string); ok {
			version = ver
		}
	}
	return version
}

// normalize returns the package name from the package url
func normalize(url string) (string, error) {
	match := repositoryURLRegex.FindStringSubmatch(url)
	if len(match) > 1 {
		packageName := match[1]
		// Dealing with packages in this format: git@github.com:root/packageName.git
		packageName = strings.Replace(packageName, ":", "/", 1)
		return packageName, nil
	}
	return "", fmt.Errorf("parsing swift Packages.resolved url: %s", url)
}

// packageName returns the name of the package for different versions of the lock file
func packageName(m map[string]any) (string, error) {
	var url string
	url, ok := m["location"].(string)
	if ok {
		return normalize(url)
	}
	url, ok = m["repositoryURL"].(string)
	if ok {
		return normalize(url)
	}

	// If we don't have a location or repositoryURL we can't extract the package name
	return "", fmt.Errorf("parsing swift Packages.resolved file: %s", m)
}

// Parse scans a Package.resolved or Podfile.lock file and returns a list of packages
func Parse(r io.Reader, _ parser.Options) ([]parser.Package, []parser.Dependency, error) {
	// Parse the JSON data
	var lockfile lockfile
	if err := json.NewDecoder(r).Decode(&lockfile); err != nil {
		return nil, nil, err
	}
	return lockfile.Packages, nil, nil
}

func init() {
	parser.Register("swift", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeSwift,
		Filenames:   []string{"Package.resolved"},
	})
}
