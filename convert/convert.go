package convert

import (
	"time"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v5"
)

// Converter converts files returned by the scanner
// to Dependency Scanning reports
type Converter struct {
	AnalyzerDetails report.AnalyzerDetails
	ScannerDetails  report.ScannerDetails
	StartTime       *time.Time
	SchemaModel     uint
}

// ToReport converts dependency files returned by the scanner
// to a Dependency Scanning report
func (c Converter) ToReport(scanFiles []scanner.File) *report.Report {
	// collect vulnerabilities and dependency files
	vulns := []report.Vulnerability{}
	for _, scanFile := range scanFiles {
		// add affections as vulnerabilities
		fc := NewFileConverter(scanFile, c.SchemaModel)
		vulns = append(vulns, fc.Vulnerabilities()...)
	}

	// initialize report with vulnerabilities, dep. files, and scan object
	r := report.NewReport()
	r.Vulnerabilities = vulns
	r.Scan.Scanner = c.ScannerDetails
	r.Scan.Analyzer = c.AnalyzerDetails
	r.Scan.Type = report.CategoryDependencyScanning
	r.Scan.Status = report.StatusSuccess

	// set start time and end time if start time is configured
	if c.StartTime != nil {
		startTime := report.ScanTime(*c.StartTime)
		r.Scan.StartTime = &startTime
		endTime := report.ScanTime(time.Now())
		r.Scan.EndTime = &endTime
	}

	return &r
}
