package convert

import "encoding/json"

// These fields should be added to the report package (https://gitlab.com/gitlab-org/security-products/analyzers/report)
// however, they've been added here as a workaround for this issue: https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/merge_requests/313#note_939260642
// TODO: move these fields to the report package once https://gitlab.com/gitlab-org/gitlab/-/issues/361768 has been completed
// This should be implemented in https://gitlab.com/gitlab-org/gitlab/-/issues/465969

// DetailsNamelessTextField stores a raw text detail type without a required name field
// https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/v14.1.1/src/vulnerability-details-format.json#L123-138
type DetailsNamelessTextField struct {
	Value string `json:"value"`
}

// DetailsTextListField stores a raw list detail type
// https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/v14.1.1/dist/dependency-scanning-report-format.json#L92-110
type DetailsTextListField struct {
	Name  string                     `json:"name"`
	Items []DetailsNamelessTextField `json:"items"`
}

// MarshalJSON turns a DetailsListField into a json object
func (d DetailsTextListField) MarshalJSON() ([]byte, error) {
	type rawType DetailsTextListField
	var typed = struct {
		Type string `json:"type"`
		rawType
	}{
		"list",
		rawType(d),
	}

	return json.Marshal(typed)
}

// MarshalJSON turns a DetailsNamelessTextField into a json object
func (d DetailsNamelessTextField) MarshalJSON() ([]byte, error) {
	type rawType DetailsNamelessTextField
	var typed = struct {
		Type string `json:"type"`
		rawType
	}{
		"text",
		rawType(d),
	}

	return json.Marshal(typed)
}
